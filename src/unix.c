/* src/unix.c: UNIX dependent code.					-CJS-

   Copyright (c) 1989-91 James E. Wilson, Christopher J. Stuart

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Library General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/


#include "config.h"
/* defines NULL */
#include <stdio.h>
/* defines CTRL */
/* defines TRUE and FALSE */
#include <curses.h>

#include "constant.h"
#include "types.h"


#include <signal.h>

#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <string.h>
#include <fcntl.h>

/* This must be included after fcntl.h, which has a prototype for `open'
   on some systems.  Otherwise, the `open' prototype conflicts with the
   `topen' declaration.  */
#include "externs.h"

/* Provides for a timeout on input. Does a non-blocking read, consuming the
   data if any, and then returns 1 if data was read, zero otherwise.

   Porting:

   In systems without the select call, but with a sleep for
   fractional numbers of seconds, one could sleep for the time
   and then check for input.

   In systems which can only sleep for whole number of seconds,
   you might sleep by writing a lot of nulls to the terminal, and
   waiting for them to drain, or you might hack a static
   accumulation of times to wait. When the accumulation reaches a
   certain point, sleep for a second. There would need to be a
   way of resetting the count, with a call made for commands like
   run or rest. */
int
check_input (microsec)
     int microsec;
{
#if ! WINDOWS
  struct timeval tbuf;
  int ch;
  fd_set smask;

  /* Return true if a read on descriptor 1 will not block. */
  tbuf.tv_sec = 0;
  tbuf.tv_usec = microsec;
  FD_ZERO (&smask);
  FD_SET (fileno (stdin), &smask);
  if (select (1, &smask, (fd_set *) 0, (fd_set *) 0, &tbuf) == 1)
    {
      ch = getch ();
      /* check for EOF errors here, select sometimes works even when EOF */
      if (ch == -1)
	{
	  eof_flag++;
	  return 0;
	}
      return 1;
    }
  else
    return 0;
#endif
  return 1;
}
